package com.sigma.technology.gwInterface;

/**
 * @author Rashid Darwish <Rashid.Darwish@gmail.com>, Nakyanzi Lynnie <gnlynnie@gmail.com>
 * @version 1.0
 * @since 2016-03-19
 */
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.nio.charset.StandardCharsets;

import org.json.JSONObject;

public class GwInterface {

    private Socket socket;
    private OutputStreamWriter outputStream;
    private BufferedReader bufferReader;
    private static GwInterface instance = null;
    private final Object mutex = new Object();

    private GwInterface() {}

    public static GwInterface getInstance() {
        if (instance == null) {
            instance = new GwInterface();
        }
        return instance;
    }

    private void initializeLinkClient() {
        try {
            socket = new Socket("localhost", 11000);
            outputStream = new OutputStreamWriter(socket.getOutputStream(), StandardCharsets.UTF_8);
            bufferReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));

        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
    }

    private void closeSocket() {
        try {
            outputStream.flush();
            outputStream.close();
            bufferReader.close();
            socket.close();
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
    }

    public String excuteAction(JSONObject jsonObj) {
        synchronized (mutex) {
            String response = null;
            try {
                Thread.sleep(250);
                initializeLinkClient();
                String jsonObjAsStr = jsonObj.toString() + "\n";
                outputStream.write(jsonObjAsStr);
                outputStream.flush();
                System.err.println(">>>>> SENT TO MBT SERVER: " + jsonObj);

                if ((response = bufferReader.readLine()) != null) {
                    if (response.equals("CONTINUE") || response.equals("COMPLETED")) {
                        closeSocket();
                        System.out.println("*****RESPONSE FROM MBT SERVER: " + response);
                    }
                }
            } catch (IOException e) {
                System.err.println(e.getMessage());
            } catch (InterruptedException e) {
                System.err.println(e.getMessage());
            }
            return response;
        }
    }

    public void stop() {
        String quitCommand = "QUIT";
        JSONObject obj = new JSONObject();
        obj.put(quitCommand, quitCommand);
        excuteAction(obj);
        instance = null;
    }
}
