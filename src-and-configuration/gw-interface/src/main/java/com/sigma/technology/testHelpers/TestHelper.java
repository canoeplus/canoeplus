package com.sigma.technology.testHelpers;

/**
 * @author Rashid Darwish <Rashid.Darwish@gmail.com>, Nakyanzi Lynnie <gnlynnie@gmail.com>
 * @version 1.0
 * @since 2016-03-19
 */
public class TestHelper {

    /**
     * Get Method Name.
     * <p>
     * Returns the name of the calling Method.
     * <p>
     * 
     * @param N/A
     * @return String of the Method Name.
     */
    public static String getMethodName() {
        return Thread.currentThread().getStackTrace()[2].getMethodName();
    }
}
