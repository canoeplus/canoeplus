package com.sigma.technology.fsm;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.concurrent.TimeUnit;

import org.graphwalker.core.condition.EdgeCoverage;
import org.graphwalker.core.condition.ReachedVertex;
import org.graphwalker.core.condition.TimeDuration;
import org.graphwalker.core.generator.AStarPath;
import org.graphwalker.core.generator.RandomPath;
import org.graphwalker.core.machine.ExecutionContext;
import org.graphwalker.java.annotation.Edge;
import org.graphwalker.java.annotation.GraphWalker;
import org.graphwalker.java.annotation.Vertex;
import org.graphwalker.java.test.TestBuilder;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

import com.sigma.technology.gwInterface.GwInterface;
import com.sigma.technology.testHelpers.TestHelper;

public class TFSModelTest extends ExecutionContext implements TFSModel {

    public final static Path MODEL_PATH = Paths.get("com/sigma/technology/fsm/TFSModel.graphml");

    private GwInterface gwInterface;
    private final static String METHOD = "Method";
    private final static String ignitionStatus = "IgnitionStatus";
    private final static String winDown = "WinDown";
    private final static String winUp = "WinUp";
    private final static String LockStatus = "LockStatus";
    private final static String LightStatus = "LightStatus";

    public TFSModelTest() {
      gwInterface = GwInterface.getInstance();
    }

    @Override
    public void e_Init() {
        JSONObject obj = new JSONObject();
        obj.put(METHOD, TestHelper.getMethodName());
        obj.put(ignitionStatus, getExpectedValue(ignitionStatus));
        obj.put(LightStatus, getExpectedValue(LightStatus));

        System.out.println(obj.toString());
        gwInterface.excuteAction(obj);
    }

    @Override
    public void v_Locked() {
        JSONObject obj = new JSONObject();
        obj.put(METHOD, TestHelper.getMethodName());
        obj.put(ignitionStatus, getExpectedValue(ignitionStatus));
        obj.put(LockStatus, getExpectedValue(LockStatus));
        System.out.println(obj.toString());
        gwInterface.excuteAction(obj);
    }
    
    @Override
    public void e_LockDoor() {
        JSONObject obj = new JSONObject();
        obj.put(METHOD, TestHelper.getMethodName());
        obj.put(ignitionStatus, getExpectedValue(ignitionStatus));
        obj.put(LockStatus, getExpectedValue(LockStatus));

        System.out.println(obj.toString());
        gwInterface.excuteAction(obj);
    }

    @Override
    public void v_Unlocked() {
        JSONObject obj = new JSONObject();
        obj.put(METHOD, TestHelper.getMethodName());
        obj.put(ignitionStatus, getExpectedValue(ignitionStatus));
        obj.put(LockStatus, getExpectedValue(LockStatus));

        System.out.println(obj.toString());
        gwInterface.excuteAction(obj);
    }

    @Override
    public void e_UnlockDoor() {
        JSONObject obj = new JSONObject();
        obj.put(METHOD, TestHelper.getMethodName());
        obj.put(ignitionStatus, getExpectedValue(ignitionStatus));
        obj.put(LockStatus, getExpectedValue(LockStatus));

        System.out.println(obj.toString());
        gwInterface.excuteAction(obj);
    }

    @Override
    public void v_WindowClose() {
        JSONObject obj = new JSONObject();
        obj.put(METHOD, TestHelper.getMethodName());

        System.out.println(obj.toString());
        gwInterface.excuteAction(obj);

    }

    @Override
    public void v_WindowOpen() {
        JSONObject obj = new JSONObject();
        obj.put(METHOD, TestHelper.getMethodName());

        System.out.println(obj.toString());
        gwInterface.excuteAction(obj);
    }

    @Override
    public void e_RollWindowUp() {
        JSONObject obj = new JSONObject();
        obj.put(METHOD, TestHelper.getMethodName());
        obj.put(winUp, getExpectedValue(winUp));

        System.out.println(obj.toString());
        gwInterface.excuteAction(obj);

    }

    @Override
    public void e_RollWindowDown() {
        JSONObject obj = new JSONObject();
        obj.put(METHOD, TestHelper.getMethodName());
        obj.put(winDown, getExpectedValue(winDown));

        System.out.println(obj.toString());
        gwInterface.excuteAction(obj);
    }

    @Override
    public void v_IgnitionStatusOn() {

        JSONObject obj = new JSONObject();
        obj.put(METHOD, TestHelper.getMethodName());
        obj.put(ignitionStatus, getExpectedValue(ignitionStatus));

        System.out.println(obj.toString());
        gwInterface.excuteAction(obj);
    }

    @Override
    public void e_TurnOffIgnition() {
        JSONObject obj = new JSONObject();
        obj.put(METHOD, TestHelper.getMethodName());
        obj.put(ignitionStatus, getExpectedValue(ignitionStatus));

        System.out.println(obj.toString());
        gwInterface.excuteAction(obj);
    }

    @Override
    public void v_IgnitionStatusOff() {
        JSONObject obj = new JSONObject();
        obj.put(METHOD, TestHelper.getMethodName());
        obj.put(ignitionStatus, getExpectedValue(ignitionStatus));

        System.out.println(obj.toString());
        gwInterface.excuteAction(obj);
    }

    @Override
    public void e_TurnOnIgnition() {
        JSONObject obj = new JSONObject();
        obj.put(METHOD, TestHelper.getMethodName());
        obj.put(ignitionStatus, getExpectedValue(ignitionStatus));

        System.out.println(obj.toString());
        gwInterface.excuteAction(obj);
    }

    @Override
    public void e_TurnLightOn() {
        JSONObject obj = new JSONObject();
        obj.put(METHOD, TestHelper.getMethodName());
        obj.put(LightStatus, getExpectedValue(LightStatus));

        System.out.println(obj.toString());
        gwInterface.excuteAction(obj);
    }

    @Override
    public void e_TurnLightOff() {
        JSONObject obj = new JSONObject();
        obj.put(METHOD, TestHelper.getMethodName());
        obj.put(ignitionStatus, getExpectedValue(ignitionStatus));
        obj.put(LightStatus, getExpectedValue(LightStatus));

        System.out.println(obj.toString());
        gwInterface.excuteAction(obj);

    }

    @Override
    public void v_LightOn() {
        JSONObject obj = new JSONObject();
        obj.put(METHOD, TestHelper.getMethodName());
        obj.put(ignitionStatus, getExpectedValue(ignitionStatus));
        obj.put(LightStatus, getExpectedValue(LightStatus));
        System.out.println(obj.toString());
        gwInterface.excuteAction(obj);
    }

    @Override
    public void v_LightOff() {
        JSONObject obj = new JSONObject();
        obj.put(ignitionStatus, getExpectedValue(ignitionStatus));
        obj.put(LightStatus, getExpectedValue(LightStatus));
        obj.put(METHOD, TestHelper.getMethodName());
        
        System.out.println(obj.toString());
        gwInterface.excuteAction(obj);
    }

    private String getExpectedValue(String attributeName) {
        Integer value = ((Integer) getAttribute(attributeName)).intValue();
        return value.toString();
    }

    @Test
    public void runFunctionalTest() {
        new TestBuilder()
                .setModel(MODEL_PATH)
                .setContext(new TFSModelTest())
                .setPathGenerator(new RandomPath(new EdgeCoverage(100)))
                .setStart("e_Init")
                .execute();
        gwInterface.stop();
    }

    //    @Test
    //    public void runSmokeTest() {
    //        new TestBuilder()
    //                .setModel(MODEL_PATH)
    //                .setContext(new EngineTest())
    //                .setPathGenerator(new AStarPath(new ReachedVertex("v_EngineOn")))
    //                .setStart("e_Init")
    //                .execute();
    //    }

    //    @Test
    //    public void runStabilityTest() {
    //        new TestBuilder()
    //                .setModel(MODEL_PATH)
    //                .setContext(new EngineTest())
    //                .setPathGenerator(new RandomPath(new TimeDuration(30, TimeUnit.SECONDS)))
    //                .setStart("e_Init")
    //                .execute();
    //    }
}
