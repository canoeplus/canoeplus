using System;
using Vector.Tools;
using Vector.CANoe.Runtime;
using Vector.CANoe.Sockets;
using Vector.CANoe.Threading;
using Vector.Diagnostics;
using Vector.Scripting.UI;
using Vector.CANoe.TFS;
using Vector.CANoe.VTS;
using NetworkDB;


public class NETTFS : TestModule
{
    const int kWAIT_TIMEOUT = 500; // 500msecs
    long WaitResult;

    public override void Main()
    {
        int count = 0;
        /* Add Test Groups/Cases */
        TestGroupBegin("Simulation of Signals", "Test node communication");
        TC_IgnitionTest(); // Ignition Test Case

        // Rolling Windows Down and Up
        while (count != 16)
        {
            TC_WindowDown(count);
            Execution.Wait(kWAIT_TIMEOUT);
            count++;
        }

        while (count != 0)
        {
            TC_WindowUp(count);
            Execution.Wait(kWAIT_TIMEOUT);
            count--;
        }

        TC_WindowUp(0);
        Execution.Wait(kWAIT_TIMEOUT);

        //Test Lock
        TC_TestLock();

        //Test Light
        TC_TestLight();

        //Wait 10secs before end
        TC_Wait10sec();
        TestGroupEnd();

    }
    [TestCase("Test Step 1", "Set envVar signal 'Light_Rq' = 1")]
    private void TC_TestLight()
    {
        // Test case & step #1 description
        Report.TestCaseTitle("", "Test Light");
        Report.TestStep("Test Step 1", "Set envVar signal 'LightRq' = 1");

        // Set �LightRq� = 1
        LightRqEnv.Value = 1;

        // Wait for the message �LightSysState� to occur for 500ms
        WaitResult = Execution.Wait<LightState>(1, kWAIT_TIMEOUT);

        // Determine results of test step
        switch (WaitResult)
        {
            case 0: Report.TestStepFail("Test Step 1", "Message not received");
                break;

            case 1:
                // If Lock state is light (=1), then test passed.
                if (LightState.Value == 1) Report.TestStepPass("Light Passed!");
                else Report.TestStepFail("Light Still OFF");
                break;
        }

        Execution.Wait(kWAIT_TIMEOUT); // Wait for 500ms
        // Test step #2 description
        Report.TestStep("Test Step 2", "Set envVar signal 'LockRq' = 0");
        // Set LockRq = 0
       LightRqEnv.Value = 0;
        // Wait for the message �LockingSysState� to occur for 500ms
        WaitResult = Execution.Wait<LightState>(0, kWAIT_TIMEOUT);
        // Determine results of test step
        switch (WaitResult)
        {
            case 0: Report.TestStepFail("Test Step 2", "Message not received");
                break;

            case 1: // Retrieve the message data of �LockSysState�
                // If Lock state is unlocked (=0), then test passed.
                if (LightState.Value == 0) Report.TestStepPass("Light Passed!");
                else Report.TestStepFail("Still Lighted");
                break;
        }
        Execution.Wait(kWAIT_TIMEOUT); // Wait for 500ms
    }

    [TestCase("Wait", "10sec Wait Period")]
    private void TC_Wait10sec()
    {
        // Wait for 10 seconds
        Execution.Wait(10000);
    }

    [TestCase("Test Step 1", "Set envVar signal 'LockRq' = 1")]
    private void TC_TestLock()
    {
        // Test case & step #1 description
        Report.TestCaseTitle("", "Test Lock");
        Report.TestStep("Test Step 1", "Set envVar signal 'LockRq' = 1");

        // Set �LockRq� = 1
        LockRqEnv.Value = 1;

        // Wait for the message �LockingSysState� to occur for 500ms
        WaitResult = Execution.Wait<LockState>(1, kWAIT_TIMEOUT);

        // Determine results of test step
        switch (WaitResult)
        {
            case 0: Report.TestStepFail("Test Step 1", "Message not received");
                break;

            case 1:
                // If Lock state is locked (=1), then test passed.
                if (LockState.Value == 1) Report.TestStepPass("Locking Passed!");
                else Report.TestStepFail("Still Unlocked");
                break;
        }

        Execution.Wait(kWAIT_TIMEOUT); // Wait for 500ms
        // Test step #2 description
        Report.TestStep("Test Step 2", "Set envVar signal 'LockRq' = 0");
        // Set LockRq = 0
        LockRqEnv.Value = 0;
        // Wait for the message �LockingSysState� to occur for 500ms
        WaitResult = Execution.Wait<LockState>(0, kWAIT_TIMEOUT);
        // Determine results of test step
        switch (WaitResult)
        {
            case 0: Report.TestStepFail("Test Step 2", "Message not received");
                break;

            case 1: // Retrieve the message data of �LockSysState�
                // If Lock state is unlocked (=0), then test passed.
                if (LockState.Value == 0) Report.TestStepPass("Unlocking Passed!");
                else Report.TestStepFail("Still Locked");
                break;
        }
        Execution.Wait(kWAIT_TIMEOUT); // Wait for 500ms
    }


    [TestCase("Rolling Windows", "Window Up")]
    private void TC_WindowUp(int value)
    {
        // Test case title description
        Report.TestCaseTitle("Rolling Windows", "Window Up");
        // �WindowDown� = 1
        WindowUpEnv.Value = 1;
        // Retrieve message content �WindowState�
        Report.TestCaseMeasuredValue(">>>>> @ 1 WindowStatus: ", WindowStatus.Value);
        WaitResult = Execution.Wait<WindowStatus>(1, kWAIT_TIMEOUT);
        Report.TestCaseMeasuredValue(">>>>> @ 2 WindowStatus: ", WindowStatus.Value);
        Report.TestCaseMeasuredValue(">>>>> @ 1 WaitResult: ", WaitResult);
        // Determine results of test step

        switch (WaitResult)
        {
            case 0: Report.TestStepFail("Rolling Windows", "Message not received");
                break;
            case 1: // Retrieve the message data of �WinStat�
                // If current window position = theoretical window position, then test
                // step has passed. Also, take a screenshot of the Data window.
                Report.TestCaseMeasuredValue(">>>>> @ WindowPosition is [: ", WindowPosition.Value + "] While Expecting [" + value + "]");
                if (WindowPosition.Value == value)
                {
                    Report.TestStepPass("Rolling Window Up Passed!");
                }

                else Report.TestStepFail("Window Position is incorrect!");
                break;
        }
        // Make sure to reset Window Up button to original position
        WindowUpEnv.Value = 0;
    }

    [TestCase("Rolling Windows", "Window Down")]
    private void TC_WindowDown(int value)
    {
        // Test case title description
        // �WindowDown� = 1
        WindowDownEnv.Value = 1;
        // Retrieve message content �WindowState�
        Report.TestCaseMeasuredValue(">>>>> @ 1 WindowStatus: ", WindowStatus.Value);
        WaitResult = Execution.Wait<WindowStatus>(2, kWAIT_TIMEOUT);
        Report.TestCaseMeasuredValue(">>>>> @ 2 WindowStatus: ", WindowStatus.Value);
        Report.TestCaseMeasuredValue(">>>>> @ 1 WaitResult: ", WaitResult);
        // Determine results of test step

        switch (WaitResult)
        {

            case 0: Report.TestStepFail("Test Step 2", "Message not received");
                break;
            case 1: // Retrieve the message data of �WinStat�
                // If current window position = theoretical window position, then test
                // step has passed. Also, take a screenshot of the Trace window
                Report.TestCaseMeasuredValue(">>>>> @ WindowPosition is [: ", WindowPosition.Value + "] While Expecting [" + value + "]");
                if (WindowPosition.Value == value)
                {
                    Report.TestStepPass("Rolling Window Down Passed!");
                }
                else Report.TestStepFail("Window Position is incorrect!");
                break;
        }
        // Make sure to reset Window Down button to original position
        WindowDownEnv.Value = 0;

    }

    [TestCase("Ignition Start/Stop Check", "Ignition test")]
    private void TC_IgnitionTest()
    {
        float IgnitStart_Value;

        // Set Test case title and test step #1 description
        Report.TestCaseTitle("Ignition Start/Stop Check", "Ignition test");
        Report.TestStep("Test Step 1", "Set envVar signal 'IgnitionStart' = 0");

        // Set �IgnitionStart� = 0
        IgnitionStartEnv.Value = 0;

        // Wait for the environment variable �IgnitionStart� to occur for 500ms
        WaitResult = Execution.Wait<IgnitionStartEnv>(0, kWAIT_TIMEOUT);

        // Determine results of test step
        switch (WaitResult)
        {
            case 0: Report.TestStepFail("Test Step 1", "envVar not received!");
                break;

            case 1: // Retrieve the value of �IgnitionStart� & store in �IgnitStart_Value�
                IgnitStart_Value = IgnitionStartEnv.Value;
                if (IgnitStart_Value == 0) Report.TestStepPass("Test Step 2", "Switch is working!");
                else Report.TestStepFail("Test Step 1", "Switch not working!");
                break;
        }

        Execution.Wait(kWAIT_TIMEOUT); // Wait for 500ms

        // Set test step #2 description
        Report.TestStep("Test Step 2", "Set envVar signal 'IgnitionStart' = 1");

        // Set �IgnitionStart� = 1
        IgnitionStartEnv.Value = 1;

        // Wait for the message �IgnitionStatus� to occur for 500ms
        WaitResult = Execution.Wait<Ignition_Status>(1, kWAIT_TIMEOUT);

        // Determine results of test step
        switch (WaitResult)
        {
            case 0: Report.TestStepFail("Test Step 2", "Signal not received");
                break;

            case 1: // Retrieve the message data of �IgnitStat�

                if (Ignition_Status.Value == 1) Report.TestStepPass("Ignition on!");
                else Report.TestStepFail("Ignition off!");
                break;
        }

        Execution.Wait(kWAIT_TIMEOUT); // Wait for 500ms
    }
}
