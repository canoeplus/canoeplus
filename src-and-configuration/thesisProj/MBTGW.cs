using System;
using Vector.Tools;
using Vector.CANoe.Runtime;
using Vector.CANoe.Sockets;
using Vector.CANoe.Threading;
using Vector.Diagnostics;
using Vector.Scripting.UI;
using Vector.CANoe.TFS;
using Vector.CANoe.VTS;
using NetworkDB;
using MBT;



public class MBTGW : TestModule
{
    private const String IgnitionStatusExpectedValue = "IgnitionStatus";
    private const String LightStatusExpectedValue = "LightStatus";
    //private const String LockStatusExpectedValue = "LockStatus";
    //private const String WinDownExpectedValue = "WinDown";
    //private const String WindowUpExpectedValue = "WindowUp";

    private int windowPosition = 0;

    private const int kWAIT_TIMEOUT = 500; // 500msecs

    public override void Main()
    {
        GwLinkServer.Instance.TestModule = this;
        GwLinkServer.Instance.startMBT();
    }

    private int getExpectedValue(string attributeName)
    {
        return int.Parse(GwLinkServer.Instance.getAttributeValue(attributeName));
    }



    [TestCase("Model-Based Testing: Setup preconditions")]
    public void e_Init()
    {
        Report.TestCaseTitle("Init", "Setup Test Preconditions");
        NetworkDB.IgnitionStartEnv.Value = getExpectedValue(IgnitionStatusExpectedValue);
        NetworkDB.LightRqEnv.Value = getExpectedValue(LightStatusExpectedValue);
        Execution.Wait(kWAIT_TIMEOUT);
    }

    [TestCase("Model-Based Testing: e_turnOffIgnition ")]
    public void e_TurnOffIgnition()
    {
        Report.TestCaseTitle("Turning Ignition OFF", "Turning Ignition OFF");
        NetworkDB.IgnitionStartEnv.Value = getExpectedValue(IgnitionStatusExpectedValue);
        Report.TestCaseMeasuredValue(NetworkDB.IgnitionStartEnv.Value.ToString(), " While ExpectedValue Ignition is: " + getExpectedValue(IgnitionStatusExpectedValue));
        long WaitResult = Execution.Wait<IgnitionStartEnv>(0, kWAIT_TIMEOUT);
        // Determine results of test step
        switch (WaitResult)
        {
            case 0: Report.TestStepFail("Turn OFF Ignition", "Message not received");
                break;
            case 1: Report.TestStepPass("Turn OFF Ignition PASSED!");
                break;
        }
        Execution.Wait(kWAIT_TIMEOUT);
    }

    [TestCase("Model-Based Testing: v_IgnitionStatusOn ")]
    public void v_IgnitionStatusOn()
    {
        Report.TestCaseTitle("Ignition Status ON", "Ignition Status ON");
        int expectedValue = getExpectedValue(IgnitionStatusExpectedValue);
        if (NetworkDB.Ignition_Status.Value == expectedValue)
        {
            Report.TestStepPass("Engine is running.");
        }
        else
        {
            Report.TestStepFail("Engine is not running.");
        }
    }

    [TestCase("Model-Based Testing: v_IgnitionStatusOff ")]
    public void v_IgnitionStatusOff()
    {
        Report.TestCaseTitle("Ignition Status OFF", "Ignition Status OFF");
        int expectedValue = getExpectedValue(IgnitionStatusExpectedValue);
        if (NetworkDB.Ignition_Status.Value == expectedValue)
        {
            Report.TestStepPass("Engine is not running.");
        }
        else
        {
            Report.TestStepFail("Engine is running.");
        }
    }

    [TestCase("Model-Based Testing: e_turnOnIgnition ")]
    public void e_TurnOnIgnition()
    {
        Report.TestCaseTitle("Turning Ignition ON", "Turning Ignition ON");
        NetworkDB.IgnitionStartEnv.Value = getExpectedValue(IgnitionStatusExpectedValue);
        long WaitResult = Execution.Wait<Ignition_Status>(1, kWAIT_TIMEOUT);
        // Determine results of test step
        switch (WaitResult)
        {
            case 0: Report.TestStepFail("Turn ON Ignition", "Message not received");
                break;
            case 1: Report.TestStepPass("Turn ON Ignition PASSED!");
                break;
        }
        Execution.Wait(kWAIT_TIMEOUT);
    }

    [TestCase("Model-Based Testing: e_rollWindowUp ")]
    public void e_RollWindowUp()
    {
        Report.TestCaseTitle("Rolling Windows UP", "Rolling Window Up");
        WindowUpEnv.Value = 1;
        long WaitResult = Execution.Wait<WindowStatus>(1, kWAIT_TIMEOUT);

        // Determine results of test step
        switch (WaitResult)
        {
            case 0: Report.TestStepFail("Rolling Windows", "Message not received");
                break;
            case 1: Report.TestStepPass("Rolling Window Up Passed!");
                windowPosition--;
                break;
        }
        Execution.Wait(kWAIT_TIMEOUT);
    }

    [TestCase("Model-Based Testing: e_rollWindowDown ")]
    public void e_RollWindowDown()
    {
        Report.TestCaseTitle("Rolling Windows Down", "Rolling Windows Down");
        WindowDownEnv.Value = 1;
        long WaitResult = Execution.Wait<WindowStatus>(2, kWAIT_TIMEOUT);

        // Determine results of test step
        switch (WaitResult)
        {

            case 0: Report.TestStepFail("Test Step 2", "Message not received");
                break;
            case 1: Report.TestStepPass("Rolling Window Down Passed!");
                windowPosition++;
                break;
        }
        Execution.Wait(kWAIT_TIMEOUT);
    }

    [TestCase("Model-Based Testing: v_WindowClose ")]
    public void v_WindowClose()
    {
        Report.TestCaseTitle("Windows Status Close", "Window Position State Close");
        Report.TestStep("v_WindowClose");
        // Determine results of test step
        if (WindowPosition.Value == windowPosition)
        {
            Report.TestStepPass("Rolling Window Close Position is correct!");
        }
        else
        {
            Report.TestStepFail("Rolling Window Close Position is incorrect!");
        }

        // Make sure to reset Window Up button to original position
        WindowUpEnv.Value = 0;
        Execution.Wait(kWAIT_TIMEOUT);
    }

    [TestCase("Model-Based Testing: v_WindowOpen ")]
    public void v_WindowOpen()
    {
        Report.TestCaseTitle("Windows Status Open", "Window Position State Open");

        if (WindowPosition.Value == windowPosition)
        {
            Report.TestStepPass("Rolling Window Open Position is correct!");
        }
        else
        {
            Report.TestStepFail("Rolling Window Open Position is incorrect!");
        }
        // Make sure to reset Window Down button to original position
        WindowDownEnv.Value = 0;
        Execution.Wait(kWAIT_TIMEOUT);
    }

    [TestCase("Model-Based Testing: e_Lock ")]
    public void e_LockDoor()
    {
        Report.TestCaseTitle("Locking Door", "Locking Door");

        // Set “LockRq” = 1
        LockRqEnv.Value = 1;

        // Wait for the message “LockingSysState” to occur for 500ms
        long WaitResult = Execution.Wait<LockState>(1, kWAIT_TIMEOUT);

        // Determine results of test step
        switch (WaitResult)
        {
            case 0: Report.TestStepFail("Locking Door", "Message not received");
                break;

            case 1: Report.TestStepPass("Locking Door msg received!");
                break;
        }
        Execution.Wait(kWAIT_TIMEOUT); // Wait for 500ms
    }

    [TestCase("Model-Based Testing: v_Locked ")]
    public void v_Locked()
    {
        Report.TestCaseTitle("Door Lock Status", "Door is Locked");

        // If Lock state is locked (=1), then test passed.
        if (LockState.Value == 1)
        {
            Report.TestStepPass("Locking Door Passed!");
        }
        else
        {
            Report.TestStepFail("Door Still Unlocked");
        }

        Execution.Wait(kWAIT_TIMEOUT); // Wait for 500ms
    }

    [TestCase("Model-Based Testing: v_Unlocked ")]
    public void v_Unlocked()
    {
        Report.TestCaseTitle("Door Lock Status", "Unlocked Door");
        if (LockState.Value == 0)
        {
            Report.TestStepPass("Door Unlocking Passed!");
        }
        else
        {
            Report.TestStepFail("Door Still Locked");
        }
        Execution.Wait(kWAIT_TIMEOUT); // Wait for 500ms
    }

    [TestCase("Model-Based Testing: e_Unlock ")]
    public void e_UnlockDoor()
    {
        Report.TestCaseTitle("UnLocing Door", "Door UnLocked");
        LockRqEnv.Value = 0;
        // Wait for the message “LockingSysState” to occur for 500ms
        long WaitResult = Execution.Wait<LockState>(0, kWAIT_TIMEOUT);
        // Determine results of test step
        switch (WaitResult)
        {
            case 0: Report.TestStepFail("Unlocking Door", "Message not received");
                break;

            case 1:
                Report.TestStepPass("UnLocking Door msg received!");
                break;
        }
        Execution.Wait(kWAIT_TIMEOUT); // Wait for 500ms
    }

    [TestCase("Model-Based Testing: e_LightOn ")]
    public void e_TurnLightOn()
    {
        Report.TestCaseTitle("Turn Light On", "Turning Light On");
        LightRqEnv.Value = getExpectedValue(LightStatusExpectedValue);

        long WaitResult = Execution.Wait<LightState>(1, kWAIT_TIMEOUT);

        // Determine results of test step
        switch (WaitResult)
        {
            case 0: Report.TestStepFail("Turn Light On", "Message not received");
                break;

            case 1: Report.TestStepPass("Light On msg received!");
                break;
        }

        Execution.Wait(kWAIT_TIMEOUT); // Wait for 500ms
    }

    [TestCase("Model-Based Testing: e_LightOff ")]
    public void e_TurnLightOff()
    {
        Report.TestCaseTitle("Turn Light OFF", "Turning Light OFF");
        LightRqEnv.Value = getExpectedValue(LightStatusExpectedValue);
        long WaitResult = Execution.Wait<LightState>(1, kWAIT_TIMEOUT);
        // Determine results of test step
        switch (WaitResult)
        {
            case 0: Report.TestStepFail("Turn Light OFF", "Message not received");
                break;

            case 1:
                Report.TestStepPass("Light OFF msg received!");
                break;
        }
        Execution.Wait(kWAIT_TIMEOUT); // Wait for 500ms
    }

    [TestCase("Model-Based Testing: v_LightOn ")]
    public void v_LightOn()
    {
        Report.TestCaseTitle("Light Status ON", "Light is On");
        int expectedValue = getExpectedValue(LightStatusExpectedValue);

        // If Lock state is locked (=1), then test passed.
        if (LightState.Value == expectedValue)
        {
            Report.TestStepPass("Light ON Passed!");
        }
        else
        {
            Report.TestStepFail("Light Still OFF");
        }

        Execution.Wait(kWAIT_TIMEOUT); // Wait for 500ms
    }

    [TestCase("Model-Based Testing v_LightOff ")]
    public void v_LightOff()
    {
        Report.TestCaseTitle("Light Status OFF", "Light is OFF");
        int expectedValue = getExpectedValue(LightStatusExpectedValue);
        if (LightState.Value == expectedValue)
        {
            Report.TestStepPass("Light OFF Passed!");
        }
        else
        {
            Report.TestStepFail("Light Still On");
        }
        Execution.Wait(kWAIT_TIMEOUT); // Wait for 500ms
    }
}