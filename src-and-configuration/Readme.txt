This folder contains:

ecuMBT project:  
This project is the actuall test that reads the visual model (.graphml)format, and generats testcases from the model. Inorder to test the ECU in CANoe, this project uses gw-interface (edit the pom.xml to locate the path to GwInterface).

gw-interface project: 
This project the Java side interface, takes json object and transport it to LinkClient. JAR file of this project shall be used in the ecuMBT.

Vector.MBT.LinkClient Project:
This project is written in C#, its an server, runs when tests are excuted in CANoe. Build the project and the generated dll located in "debug/bin" shall be added to the test-module to test the ECU. This server recieves msg in JSON format and the server excutes it.

thesisProj: 
Contains the CANoe configuration and tests used during the development.
   