﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Vector.CANoe.TFS;

namespace MBT
{
    public sealed class GwLinkServer
    {
        private Socket listenerSocket = null;
        private IPEndPoint localEndPoint = null;
        private static volatile GwLinkServer instance;
        private static object syncRoot = new Object();
        private static object lockThis = new Object();
        private static object testModule = null;
        private Socket clientHandler = null;
        private Dictionary<string, string> attributeValueDic =
            new Dictionary<string, string>();


        private GwLinkServer()
        {
            initializeGwLinkServer();
        }

        public static GwLinkServer Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                        {
                            instance = new GwLinkServer();
                        }
                    }
                }

                return instance;
            }
        }

        public object TestModule
        {
            get
            {
                return testModule;
            }
            set
            {
                if (value != null)
                {
                    testModule = value;
                }
            }
        }

        public string getAttributeValue(string attribute)
        {
            return attributeValueDic[attribute];
        }

        private void sendToClient(Socket clientHandler, string attribute)
        {
            clientHandler.Send(Encoding.ASCII.GetBytes(attribute));
            shutdownSocketHandler(clientHandler);
        }

        private void initializeGwLinkServer()
        {
            // Establish the local endpoint for the socket.
            // Dns.GetHostName returns the name of the 
            // host running the application.
            IPHostEntry ipHostInfo = Dns.GetHostEntry("localhost");
            IPAddress ipAddress = ipHostInfo.AddressList[1];
            Console.WriteLine("ipHostInfo: " + ipAddress);
            localEndPoint = new IPEndPoint(ipAddress, 11000);

            // Create a TCP/IP socket.
            listenerSocket = new Socket(AddressFamily.InterNetwork,
                SocketType.Stream, ProtocolType.Tcp);

            listenerSocket.Bind(localEndPoint);
            listenerSocket.Listen(10);
        }

        public void startMBT()
        {
            bool quit = false;
            byte[] bytes = null;
            string data = null;
            try
            {
                // Start listening for connections.
                while (!quit)
                {
                    // Program is suspended while waiting for an incoming connection.
                    clientHandler = listenerSocket.Accept();
                    lock (lockThis)
                    {
                        data = null;
                        bytes = new byte[clientHandler.ReceiveBufferSize];
                        int bytesRec;
                        // An incoming connection needs to be processed.                 
                        while ((bytesRec = clientHandler.Receive(bytes)) != 0)
                        {
                            //bytesRec = clientHandler.Receive(bytes);
                            data = Encoding.ASCII.GetString(bytes, 0, bytesRec);

                            if (data.IndexOf("\n") > -1)
                            {
                                break;
                            }
                        }
                        JObject json = JObject.Parse(data.Replace("\n", ""));
                        if (json["QUIT"] != null)
                        {

                            sendToClient(clientHandler, "COMPLETED");
                            //shutdownSocketHandler(clientHandler);
                            quit = true;
                        }
                        else
                        {
                            processIncomingData(clientHandler, data);
                        }
                        
                    }
                    sleep(250);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                listenerSocket.Close();
                instance = null;
            }

        }

        private void processIncomingData(Socket clientHandler, string data)
        {
            JObject json = JObject.Parse(data.Replace("\n", ""));
            addAttributeToDic(json);
            invokeMethodFromModule(json.GetValue("Method").ToString());
            sendToClient(clientHandler, "CONTINUE");
            //shutdownSocketHandler(clientHandler);
        }

        private void addAttributeToDic(JObject json)
        {
            foreach (var item in json)
            {
                if (!item.Key.ToString().Equals("Method"))
                {
                    if (attributeValueDic.ContainsKey(item.Key))
                    {
                        attributeValueDic[item.Key.ToString()] = item.Value.ToString();
                    }
                    else
                    {
                        attributeValueDic.Add(item.Key.ToString(), item.Value.ToString());
                    }
                }
            }

        }

        private void sleep(int milliSeconds)
        {
            System.Threading.Thread.Sleep(milliSeconds);
        }

        private void shutdownSocketHandler(Socket socketHandler)
        {
            sleep(1000);
            socketHandler.Shutdown(SocketShutdown.Both);
            socketHandler.Close();
        }

        private void invokeMethodFromModule(string methodName)
        {
            //This part will call a function in the TestModule
            Type testModuleType = testModule.GetType();
            MethodInfo function = testModuleType.GetMethod(methodName);
            function.Invoke(testModule, null);
        }
    }
}
