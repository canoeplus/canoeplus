package com.sigma.technology.terminate;

import com.sigma.technology.gwInterface.GwInterface;

public class Terminator {

    public static void main(String[] args) {
        try {
            System.out.println("Terminator trying to connect to the server...");
            GwInterface gwInterface = GwInterface.getInstance();
            gwInterface.stop();
        } catch (NullPointerException e) {
            System.err.println("Connection Failed: " + e.getMessage());
        }
    }
}
